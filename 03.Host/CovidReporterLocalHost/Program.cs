﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StartUp;

namespace CovidReporterLocalHost
{
  class Program
  {
    static void Main(string[] args)
    {
      //This Is a simple local Host opened on port 61720
      //With the Configuration of StartUp Class From the CovidReporterApi Lib (dll)

      using (WebApp.Start<StartUp.StartUp>("http://localhost:61720"))
      {
        Console.WriteLine("WebApp Started at " + "+//:61720");
        Console.WriteLine("Press any key to quit");
        Console.ReadLine();

      }
    }
  }
}
