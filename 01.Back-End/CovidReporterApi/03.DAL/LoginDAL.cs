﻿using CovidReporterApi.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidReporterApi.DAL
{
  public class LoginDAL
  {
    public LoginDAL() { }
    public LoginDAL(SqlConnection conn, SqlTransaction trans)
    {
      _internalConnection = conn;
      _internalADOTransaction = trans;
    }

    private string _connString;
    private SqlConnection _internalConnection = null;
    private SqlTransaction _internalADOTransaction = null;

    public void ClearConnections()
    {
      SqlConnection.ClearAllPools();
    }
    public void BeginADOTransaction()
    {
      if (_internalConnection.State != ConnectionState.Open)
      {
        _internalConnection.Open();
      }

      _internalADOTransaction = _internalConnection.BeginTransaction(IsolationLevel.ReadCommitted);
    }
    public void RollBackTransaction()
    {
      try
      {
        if (_internalADOTransaction != null)
        {
          _internalADOTransaction.Rollback();
        }
      }
      catch { }
      _internalADOTransaction = null;
    }
    public void CommitADOTransaction()
    {
      if (_internalADOTransaction != null)
      {
        _internalADOTransaction.Commit();
      }
      _internalADOTransaction = null;
    }

    public void Open(string connString)
    {
      if (_internalConnection == null)
      {
        _internalConnection = new SqlConnection(connString);
      }
      if (_internalConnection.State != ConnectionState.Open)
      {
        _connString = connString;
        _internalConnection.Open();
      }
    }
    public void Close()
    {
      if (_internalConnection != null && _internalConnection.State != ConnectionState.Closed)
      {
        _internalConnection.Close();
      }
      _internalConnection = null;
    }

    const string SQL_SELECT_ACCOUNT_BY_USERNAME = "SELECT  Username, Password FROM CovidAccounts WHERE Username=@Username;";

    const string PARM_USERNAME = "@Username";

    public Account GetAccountByUsername(string username)
    {
      try
      {
        SqlParameter par = new SqlParameter(PARM_USERNAME, SqlDbType.NVarChar);
        par.Value = username;

        SqlDataReader res = SQLHelper.ExecuteReaderSingleRow(_internalConnection, _internalADOTransaction, CommandType.Text, SQL_SELECT_ACCOUNT_BY_USERNAME, par);
        Account model = null;
        if (res.Read())
        {
          model = new Account();
          model.Username = res.GetString(0);
          model.Password = res.GetString(1);
        }
        res.Close();

        return model;
      }
      catch (Exception) { throw; }
    }



  }
}
