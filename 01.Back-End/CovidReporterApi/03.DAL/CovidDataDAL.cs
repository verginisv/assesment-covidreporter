﻿using CovidReporterApi.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidReporterApi.DAL
{
  public class CovidDataDAL
  {
    public CovidDataDAL() { }
    public CovidDataDAL(SqlConnection conn, SqlTransaction trans)
    {
      _internalConnection = conn;
      _internalADOTransaction = trans;
    }

    private string _connString;
    private SqlConnection _internalConnection = null;
    private SqlTransaction _internalADOTransaction = null;

    public void ClearConnections()
    {
      SqlConnection.ClearAllPools();
    }
    public void BeginADOTransaction()
    {
      if (_internalConnection.State != ConnectionState.Open)
      {
        _internalConnection.Open();
      }

      _internalADOTransaction = _internalConnection.BeginTransaction(IsolationLevel.ReadCommitted);
    }
    public void RollBackTransaction()
    {
      try
      {
        if (_internalADOTransaction != null)
        {
          _internalADOTransaction.Rollback();
        }
      }
      catch { }
      _internalADOTransaction = null;
    }
    public void CommitADOTransaction()
    {
      if (_internalADOTransaction != null)
      {
        _internalADOTransaction.Commit();
      }
      _internalADOTransaction = null;
    }

    public void Open(string connString)
    {
      if (_internalConnection == null)
      {
        _internalConnection = new SqlConnection(connString);
      }
      if (_internalConnection.State != ConnectionState.Open)
      {
        _connString = connString;
        _internalConnection.Open();
      }
    }
    public void Close()
    {
      if (_internalConnection != null && _internalConnection.State != ConnectionState.Closed)
      {
        _internalConnection.Close();
      }
      _internalConnection = null;
    }


    const string SQL_SELECT_COVID_DATA= "SELECT Date, iso3, CountryName, lat, lon, CumulativePositive, CumulativeDeceased, CumulativeRecovered, CurrentlyPositive, Hospitalized, IntensiveCare, EUcountry, EUCPMcountry, NUTS FROM Covid19_DataSet WHERE 1=1 {filter} ;";
    const string SQL_SELECT_BARCHART = @"
                                          WITH RankedCovidData AS (
                                              SELECT 
                                                  [Date],
                                                  [iso3],
                                                  [CountryName],
                                                  [lat],
                                                  [lon],
                                                  [CumulativePositive],
                                                  [CumulativeDeceased],
                                                  [CumulativeRecovered],
                                                  [CurrentlyPositive],
                                                  [Hospitalized],
                                                  [IntensiveCare],
                                                  [EUcountry],
                                                  [EUCPMcountry],
                                                  [NUTS],
                                                  ROW_NUMBER() OVER (PARTITION BY YEAR([Date]) ORDER BY [Date] DESC) AS RowNum
                                              FROM 
                                                  [BiteBytes].[dbo].[Covid19_DataSet]
                                              WHERE 
                                                  NUTS = @NUTS
                                          ),
                                          YearlyData AS (
                                              SELECT 
                                                  YEAR([Date]) AS Year,
                                                  [Date],
                                                  [iso3],
                                                  [CountryName],
                                                  [lat],
                                                  [lon],
                                                  [CumulativePositive],
                                                  [CumulativeDeceased],
                                                  [CumulativeRecovered],
                                                  [CurrentlyPositive],
                                                  [Hospitalized],
                                                  [IntensiveCare],
                                                  [EUcountry],
                                                  [EUCPMcountry],
                                                  [NUTS]
                                              FROM 
                                                  RankedCovidData
                                              WHERE 
                                                  RowNum = 1
                                          )
                                          SELECT 
                                              yd1.Date,
                                              CAST(yd1.CumulativePositive AS BIGINT) - ISNULL(CAST(yd2.CumulativePositive AS BIGINT), 0) AS PositiveCasesForYear
                                          FROM 
                                              YearlyData yd1
                                          LEFT JOIN 
                                              YearlyData yd2 ON yd1.Year = yd2.Year + 1
                                          ORDER BY 
                                              yd1.Year DESC;";

    const string SQL_SELECT_LINECHART = @"WITH NonNullCovidData AS (
        SELECT
            [Date],
            [iso3],
            [CountryName],
            [lat],
            [lon],
            [CumulativeDeceased],
            [NUTS]
        FROM
            [BiteBytes].[dbo].[Covid19_DataSet]
        WHERE
            NUTS = @NUTS AND
            CumulativeDeceased IS NOT NULL
    ),
    MaxDeceasedData AS (
        SELECT
            YEAR([Date]) AS Year,
            MAX(CAST(CumulativeDeceased AS BIGINT)) AS MaxDeceased
        FROM
            NonNullCovidData
        GROUP BY
            YEAR([Date])
    ),
    YearlyData AS (
        SELECT DISTINCT
            YEAR(nd.[Date]) AS Year,
            nd.[Date],
            nd.[iso3],
            nd.[CountryName],
            nd.[lat],
            nd.[lon],
            CAST(nd.[CumulativeDeceased] AS BIGINT) AS CumulativeDeceased
        FROM
            NonNullCovidData nd
        JOIN
            MaxDeceasedData yd ON YEAR(nd.[Date]) = yd.Year AND nd.CumulativeDeceased = yd.MaxDeceased
    ),
    AllYears AS (
        SELECT DISTINCT YEAR([Date]) AS Year
        FROM [BiteBytes].[dbo].[Covid19_DataSet]
        WHERE NUTS = @NUTS
    )
    SELECT
        ay.Year,
        yd1.CumulativeDeceased - ISNULL(CAST(yd2.[CumulativeDeceased] AS BIGINT), 0) AS DeceasedCasesForYear
    FROM
        AllYears ay
    LEFT JOIN
        (SELECT DISTINCT Year, CumulativeDeceased FROM YearlyData) yd1 ON ay.Year = yd1.Year
    LEFT JOIN
        (SELECT DISTINCT Year, CumulativeDeceased FROM YearlyData) yd2 ON ay.Year = yd2.Year + 1
    ORDER BY
        ay.Year DESC;
";
    const string SQL_SELECT_PIECHART = @"WITH NonNullCovidData AS (
        SELECT
            [Date],
            [iso3],
            [CountryName],
            [lat],
            [lon],
            [CumulativePositive],
            [CumulativeDeceased],
            [NUTS]
        FROM
            [BiteBytes].[dbo].[Covid19_DataSet]
        WHERE
            NUTS = @NUTS
    ),
    MaxDeceasedData AS (
        SELECT
            TOP 1 CAST([CumulativeDeceased] AS BIGINT) AS CumulativeDeceased, [Date] AS DeceasedDate
        FROM
            NonNullCovidData
        WHERE
            CumulativeDeceased IS NOT NULL
        ORDER BY
            CumulativeDeceased DESC
    ),
    MaxPositiveData AS (
        SELECT
            TOP 1 CAST([CumulativePositive] AS BIGINT) AS CumulativePositive, [Date] AS PositiveDate
        FROM
            NonNullCovidData
        WHERE
            CumulativePositive IS NOT NULL
        ORDER BY
            CumulativePositive DESC
    )
    SELECT
        (SELECT CumulativeDeceased FROM MaxDeceasedData) AS MaxDeceased,
        (SELECT CumulativePositive FROM MaxPositiveData) AS MaxPositive;";


    const string PARM_NUTS = "@NUTS";

    public List<CovidData> GetTable( string filter)
    {
      try
      {
        string sql = SQL_SELECT_COVID_DATA.Replace("{filter}", filter);

        var list = new List<CovidData>();
        var rdr = SQLHelper.ExecuteReader(_internalConnection, _internalADOTransaction, CommandType.Text, sql);
        while (rdr.Read())
        {
          CovidData model = new CovidData();

          model.Date = Common.HandleDbNull<DateTime>(rdr.GetValue(0));
          model.Iso3 = rdr.GetString(1);
          model.CountryName = rdr.GetString(2);
          model.Lat = rdr.GetDecimal(3);
          model.Lon = rdr.GetDecimal(4);
          model.CumulativePositive = rdr.GetInt32(5);
          model.CumulativeDeceased = Common.HandleDbNull<int>(rdr.GetValue(6));
          model.CumulativeRecovered = Common.HandleDbNull<int>(rdr.GetValue(7));
          model.CurrentlyPositive = Common.HandleDbNull<int>(rdr.GetValue(8));
          model.Hospitalized = Common.HandleDbNull<int>(rdr.GetValue(9));
          model.IntensiveCare = Common.HandleDbNull<int>(rdr.GetValue(10));
          model.EUCountry = rdr.GetBoolean(11);
          model.EUCPMCountry = rdr.GetBoolean(12);
          model.NUTS = rdr.GetString(13);


          list.Add(model);
        }
        rdr.Close();

        return list;
      }
      catch (Exception)
      {

        throw;
      }
    }


    public List<BarChart> GetBarChart(string nuts)
    {
      try
      {
        SqlParameter par = new SqlParameter(PARM_NUTS, SqlDbType.VarChar);
        par.Value = nuts;

        var list = new List<BarChart>();
        var rdr = SQLHelper.ExecuteReader(_internalConnection, _internalADOTransaction, CommandType.Text, SQL_SELECT_BARCHART,par);
        while (rdr.Read())
        {
          BarChart model = new BarChart();
          DateTime temp = rdr.GetDateTime(0);
          string dateTostring = temp.ToString("yyyy");

          model.Year = Int32.Parse(dateTostring);
          model.Outbreaks = rdr.GetInt64(1);


          list.Add(model);
        }
        rdr.Close();

        return list;
      }
      catch (Exception ex)
      {
        throw;
      }
    }
    public List<LineChart> GetLineChart(string nuts)
    {
      try
      {
        SqlParameter par = new SqlParameter(PARM_NUTS, SqlDbType.VarChar);
        par.Value = nuts;

        var list = new List<LineChart>();
        var rdr = SQLHelper.ExecuteReader(_internalConnection, _internalADOTransaction, CommandType.Text, SQL_SELECT_LINECHART, par);
        while (rdr.Read())
        {
          LineChart model = new LineChart();

          model.Year = rdr.GetInt32(0);
          model.Deaths = Common.HandleDbNull<long>(rdr.GetValue(1));


          list.Add(model);
        }
        rdr.Close();

        return list;
      }
      catch (Exception)
      {
        throw;
      }
    }
    public PieChart GetPieChart(string nuts)
    {
      try
      {
        SqlParameter par = new SqlParameter(PARM_NUTS, SqlDbType.VarChar);
        par.Value = nuts;
 
        var rdr = SQLHelper.ExecuteReader(_internalConnection, _internalADOTransaction, CommandType.Text, SQL_SELECT_PIECHART, par);
        PieChart model = new PieChart();
        while (rdr.Read())
        {
          model.Deaths = rdr.GetInt64(0);
          model.Outbreaks = rdr.GetInt64(1);

        }
        rdr.Close();

        return model;
      }
      catch (Exception)
      {
        throw;
      }
    }

  }
}
