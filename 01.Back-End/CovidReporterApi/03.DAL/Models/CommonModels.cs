﻿using System;
using System.Net;
using System.Net.Http;

public class GeneralResponse<T>
{
  public GeneralResponse(T data, HttpStatusCode code)
  {
    Contents = data;
    Status = new HttpResponseMessage { StatusCode = code };
  }

  public T Contents { get; set; }
  public HttpResponseMessage Status { get; set; }
}


public class TableReq
{
  public DateTime? From { get; set; }
  public DateTime? To { get; set; }
  public string Nuts { get; set; }
 
}