﻿using CovidReporterApi.DAL;
using CovidReporterApi.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class LoginImplementation
{
  public bool Login(string username , string password)
  {
    LoginDAL dalo = new LoginDAL();

    try
    {
      bool check = false;
      
        dalo.Open(GetConnString());
        Account model = dalo.GetAccountByUsername(username);

        if (model != null)
          check = VerifyPassword(password, model.Password);

        if (check) { return true; }

        return false;
    }
    catch (Exception ex) { throw ex;  }
    finally { dalo.Close(); }
  }

  private bool VerifyPassword(string incPass, string accPass)
  {
    try
    {
      if (accPass == incPass)
      {
        return true;
      }
      return false;
    }
    catch (Exception) { throw; }
  }

  private string GetConnString()
  {
    return ConfigurationManager.ConnectionStrings["CovidReporter"].ConnectionString;
  }

}

