﻿using CovidReporterApi.DAL;
using CovidReporterApi.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CovidDataImplementation
{
  public List<CovidData> GetTable(TableReq filter)
  {
    List<CovidData> list = new List<CovidData>();

    CovidDataDAL dalo = new CovidDataDAL();
    try
    {
      string sqlfilter = "";

      if (!string.IsNullOrWhiteSpace(filter.Nuts) || (filter.To.HasValue && filter.From.HasValue))
      {

        sqlfilter = " AND (";
        if (!string.IsNullOrWhiteSpace(filter.Nuts)) sqlfilter += " NUTS='" + filter.Nuts + "' AND";
        if (filter.To.HasValue && filter.From.HasValue)
        {
          sqlfilter += " Date >= '" + filter.From.ToString() + "' AND Date <='" + filter.To.ToString() + "' AND";
        }

        if (sqlfilter.ToLower().Contains("select"))
          throw new Exception("SQL SELECT injection attempted");

        if (sqlfilter.ToLower().Contains("insert"))
          throw new Exception("SQL INSERT injection attempted");

        if (sqlfilter.ToLower().Contains("update"))
          throw new Exception("SQL UPDATE injection attempted");

        if (sqlfilter.ToLower().Contains("delete"))
          throw new Exception("SQL DELETE injection attempted");

        if (sqlfilter.ToLower().Contains(";"))
          throw new Exception("SQL ; injection attempted");


        sqlfilter = sqlfilter.Substring(0, sqlfilter.Length - 3) + " )";
      }
      dalo.Open(GetConnString());



      list = dalo.GetTable(sqlfilter);

      return list;
    }
    catch (Exception)
    {
      throw;
    }
    finally { dalo.Close(); }
  }

  public Dashboard GetDashboard(string nuts)
  {
    Dashboard res = new Dashboard();
    try
    {

      
      res.BarChart = GetBarChart(nuts);
      res.PieChart= GetPieChart(nuts);
      res.LineChart= GetLineChart(nuts);

      return res ;
    }
    catch (Exception ex)
    {
      throw;
    }
    finally {  }
  }

  public List<BarChart> GetBarChart(string nuts)
  {
    CovidDataDAL dalo = new CovidDataDAL();

    try
    {
      dalo.Open(GetConnString());
      List<BarChart> model = dalo.GetBarChart(nuts);
      
      return model;
    }
    catch (Exception ex) { throw ex; }
    finally { dalo.Close(); }
  }
  public List<LineChart> GetLineChart(string nuts)
  {
    CovidDataDAL dalo = new CovidDataDAL();

    try
    {
      dalo.Open(GetConnString());
      List<LineChart> model = dalo.GetLineChart(nuts);

      return model;
    }
    catch (Exception ex) { throw ex; }
    finally { dalo.Close(); }
  }
  public PieChart GetPieChart(string nuts)
  {
    CovidDataDAL dalo = new CovidDataDAL();

    try
    {
      dalo.Open(GetConnString());
      PieChart model = dalo.GetPieChart(nuts);

      return model;
    }
    catch (Exception ex) { throw ex; }
    finally { dalo.Close(); }
  }

  private string GetConnString()
  {
    return ConfigurationManager.ConnectionStrings["CovidReporter"].ConnectionString;
  }


}

