﻿using Newtonsoft.Json.Serialization;
using Owin;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace StartUp
{
  public class StartUp
  {
    public void Configuration(IAppBuilder app)
    {

      var config = new HttpConfiguration();
      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{action}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );

      config.Formatters.Remove(config.Formatters.XmlFormatter);
      config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
      config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();


      var cors = new EnableCorsAttribute("*", "*", "*");
      config.EnableCors(cors);

      app.UseWebApi(config);


      int workerThreads, completionPortThreads;
      int minWorkerThreads, maxWorkerThreads;
      int minCompletionPortThreads, maxCompletionPortThreads;

      ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);
      ThreadPool.GetMinThreads(out minWorkerThreads, out minCompletionPortThreads);
      ThreadPool.GetMaxThreads(out maxWorkerThreads, out maxCompletionPortThreads);

      ThreadPool.SetMaxThreads(workerThreads, completionPortThreads);
    }
  }

}
