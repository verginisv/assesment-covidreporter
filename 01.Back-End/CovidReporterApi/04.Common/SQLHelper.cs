﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class SQLHelper
{
  public static SqlDataReader ExecuteReader(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
  {
    using (SqlCommand cmd = new SqlCommand(cmdText, conn))
    {
      try
      {
        if (trans != null)
          cmd.Transaction = trans;

        cmd.CommandType = cmdType;
        if (cmdParms != null)
        {
          cmd.Parameters.AddRange(cmdParms);
        }
        return cmd.ExecuteReader();
      }
      catch (SqlException ex)
      {
        throw ex;
      }
    }
  }
  public static SqlDataReader ExecuteReaderSingleRow(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
  {
    using (SqlCommand cmd = new SqlCommand(cmdText, conn))
    {
      try
      {
        if (trans != null)
          cmd.Transaction = trans;

        cmd.CommandType = cmdType;
        if (cmdParms != null)
        {
          cmd.Parameters.AddRange(cmdParms);
        }
        return cmd.ExecuteReader(CommandBehavior.SingleRow);
      }
      catch (SqlException ex)
      {
        throw ex;
      }
    }
  }

  public static long ExecuteNonQuery(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
  {
    using (SqlCommand cmd = new SqlCommand(cmdText, conn))
    {
      try
      {
        if (trans != null)
          cmd.Transaction = trans;

        cmd.CommandType = cmdType;
        if (cmdParms != null)
        {
          cmd.Parameters.AddRange(cmdParms);
        }

        return cmd.ExecuteNonQuery();
      }
      catch (SqlException ex)
      {
        throw ex;
      }
    }
  }
  public static long ExecuteScalar(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
  {
    using (SqlCommand cmd = new SqlCommand(cmdText, conn))
    {
      try
      {
        if (trans != null)
          cmd.Transaction = trans;

        cmd.CommandType = cmdType;
        if (cmdParms != null)
        {
          cmd.Parameters.AddRange(cmdParms);
        }

        var res = cmd.ExecuteScalar();

        if (res != null)
        {

          return Convert.ToInt64(res);
        }
        else
        {
          return 0;
        }

      }
      catch (SqlException ex)
      {
        throw ex;
      }
    }
  }

}


