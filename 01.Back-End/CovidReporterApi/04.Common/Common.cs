﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

public static class Common
{
  public static T HandleDbNull<T>(object value)
  {
    if (value == DBNull.Value || value == null)
    {
      return default(T);
    }
    return (T)value;
  }


  public static void NullToDBNULL(SqlParameter[] parameters)
  {
    for (int i = 0; i < parameters.Length; i++)
    {
      if (parameters[i].Value == null || parameters[i].Value.ToString() == "")
      {
        parameters[i].Value = DBNull.Value;
      }
    }
  }

}
