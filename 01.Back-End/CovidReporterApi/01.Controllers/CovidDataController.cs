﻿using CovidReporterApi.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace CovidReporterApi.WebApi
{
  public class CovidDataController : ApiController
  {
    
    [HttpPost]
    public IHttpActionResult GetTable()
    {
      List<CovidData> res = new List<CovidData>();
      try
      {
        CovidDataImplementation bsl = new CovidDataImplementation();
        string data = GetRawPostData();
        if (string.IsNullOrWhiteSpace(data)) { return ResponseResult<List<CovidData>>(res, HttpStatusCode.NotFound); }

        TableReq req = Newtonsoft.Json.JsonConvert.DeserializeObject<TableReq>(data);
         res = bsl.GetTable(req);

        return ResponseResult<List<CovidData>>(res, HttpStatusCode.OK);
      }
      catch (Exception) { return ResponseResult<List<CovidData>>(res, HttpStatusCode.NotFound); throw; }
    }


    [HttpGet]
    public IHttpActionResult GetDashboard(string nuts)
    {
      Dashboard res = new Dashboard();
      try
      {
        CovidDataImplementation bsl = new CovidDataImplementation();
        if (string.IsNullOrWhiteSpace(nuts)) { return ResponseResult<Dashboard>(res, HttpStatusCode.NotFound); }
      
        res = bsl.GetDashboard(nuts);
        
        return ResponseResult<Dashboard>(res, HttpStatusCode.OK);
      }
      catch (Exception) { return ResponseResult<Dashboard>(res, HttpStatusCode.NotFound); throw; }
    }


    #region Private Operations   
    private JsonResult<GeneralResponse<T>> ResponseResult<T>(T data, HttpStatusCode code = HttpStatusCode.OK)
    {
      var response = new GeneralResponse<T>(data, code);
      var result = Json(response);
      return result;
    }
    private string GetRawPostData()
    {
      var content = Request.Content.ReadAsStringAsync().Result;
      return content;
    }
    #endregion


  }
}
