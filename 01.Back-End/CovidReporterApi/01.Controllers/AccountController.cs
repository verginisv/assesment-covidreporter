﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace CovidReporterApi.WebApi
{
  public class AccountController : ApiController
  {
    [HttpGet]
    public IHttpActionResult Login(string username, string password)
    {
      try
      {
        LoginImplementation bsl = new LoginImplementation();

        bool res = bsl.Login(username, password);

        if (!res) { return ResponseResult<bool>(false, HttpStatusCode.NotFound); }

        return ResponseResult<bool>(true, HttpStatusCode.OK);

      }
      catch (Exception)
      {
        return ResponseResult<bool>(false, HttpStatusCode.NotFound);
      }
    }




    private JsonResult<GeneralResponse<T>> ResponseResult<T>(T data, HttpStatusCode code = HttpStatusCode.OK)
    {
      var response = new GeneralResponse<T>(data, code);
      var result = Json(response);
      return result;
    }


  }
}
