﻿using System;
using System.Collections.Generic;

public class TableReq
{
  public DateTime? From { get; set; }
  public DateTime? To { get; set; }
  public string Nuts { get; set; }

  public TableReq()
  {
    Nuts = "";
    To = null;
    From = null;
  }


}
public class CovidDataModel
{
  public DateTime Date { get; set; }
  public string Iso3 { get; set; }
  public string CountryName { get; set; }
  public decimal Lat { get; set; }
  public decimal Lon { get; set; }
  public int CumulativePositive { get; set; }
  public int? CumulativeDeceased { get; set; }
  public int? CumulativeRecovered { get; set; }
  public int? CurrentlyPositive { get; set; }
  public int? Hospitalized { get; set; }
  public int? IntensiveCare { get; set; }
  public bool EUCountry { get; set; }
  public bool EUCPMCountry { get; set; }
  public string NUTS { get; set; }
}

public class Dashboard
{
  public PieChart PieChart { get; set; }
  public List<BarChart> BarChart { get; set; }
  public List<LineChart> LineChart { get; set; }
}
public class PieChart
{
  public long Outbreaks { get; set; }
  public long Deaths { get; set; }
}
public class BarChart
{
  public long Outbreaks { get; set; }
  public int Year { get; set; }
}
public class LineChart
{
  public long Deaths { get; set; }
  public int Year { get; set; }
}

