﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidReporterClient
{
  public class CovidData
  {

    public List<CovidDataModel> PopulateTable(TableReq req)
    {
      try
      {
        List<CovidDataModel> res = new List<CovidDataModel>();

        var resp = Common.Requests.Instance.Post<TableReq, GeneralResponse<List<CovidDataModel>>>(Common.UrlEnum.Table, req);

        if (resp.Status.StatusCode == System.Net.HttpStatusCode.OK)
          res = resp.Contents;

        return res;
      }
      catch (Exception)
      {
        throw;
      }
    }

    public Dashboard GetDashboard(string nuts)
    {
      try
      {
        Dashboard res = new Dashboard();
        string urlparams = $"nuts={Uri.EscapeDataString(nuts)}";
        var resp = Common.Requests.Instance.Get<GeneralResponse<Dashboard>>(Common.UrlEnum.GetDashboard, urlparams);

        if (resp.Status.StatusCode == System.Net.HttpStatusCode.OK)
        {
          res = resp.Contents;
        }

        return res;

      }
      catch (Exception) { throw; }
    }

  }
}
