﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace CovidReporterClient
{
  public class Login
  {

    public bool LogOn(string username, string password)
    {
      try
      {
        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
          return false;

        string data = $"username={Uri.EscapeDataString(username)}&password={Uri.EscapeDataString(password)}";

        var resp=Common.Requests.Instance.Get<GeneralResponse<bool>>(Common.UrlEnum.Login,data);
      
        if (resp.Status.StatusCode != System.Net.HttpStatusCode.OK)
        {
          return false;
        }

        return true;
      }
      catch (Exception) { throw; }
    }
  
  
  }


 
}


