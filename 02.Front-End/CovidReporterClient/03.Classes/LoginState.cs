﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidReporterClient
{
  public class LoginState
  {

    private bool Status;

    private static readonly Lazy<LoginState> _instance = new Lazy<LoginState>(() => new LoginState());
    public static LoginState Instance => _instance.Value;
    private LoginState()
    {
      this.Status = false;
    }


    public bool GetStatus()
    {
      return this.Status;
    }
    public void ChangeLogInStatus()
    {
      this.Status = !this.Status;
    }

  }
}
