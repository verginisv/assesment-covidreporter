﻿
namespace CovidReporterClient.Pages
{
  partial class Index
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.PanelMenu = new System.Windows.Forms.Panel();
      this.BntTables = new System.Windows.Forms.Button();
      this.BtnChart = new System.Windows.Forms.Button();
      this.PanelContent = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lblWrong = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.btnExit = new MindFusion.UI.WinForms.Button();
      this.btnLogIn = new MindFusion.UI.WinForms.Button();
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.txtUsername = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.PanelMenu.SuspendLayout();
      this.PanelContent.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // PanelMenu
      // 
      this.PanelMenu.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
      this.PanelMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.PanelMenu.Controls.Add(this.BntTables);
      this.PanelMenu.Controls.Add(this.BtnChart);
      this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Top;
      this.PanelMenu.Location = new System.Drawing.Point(0, 0);
      this.PanelMenu.Name = "PanelMenu";
      this.PanelMenu.Size = new System.Drawing.Size(1184, 26);
      this.PanelMenu.TabIndex = 0;
      // 
      // BntTables
      // 
      this.BntTables.AutoEllipsis = true;
      this.BntTables.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
      this.BntTables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BntTables.Location = new System.Drawing.Point(80, -1);
      this.BntTables.Name = "BntTables";
      this.BntTables.Size = new System.Drawing.Size(75, 26);
      this.BntTables.TabIndex = 1;
      this.BntTables.Text = "Tables";
      this.BntTables.UseVisualStyleBackColor = false;
      this.BntTables.Click += new System.EventHandler(this.BntTables_Click);
      // 
      // BtnChart
      // 
      this.BtnChart.AutoEllipsis = true;
      this.BtnChart.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
      this.BtnChart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BtnChart.Location = new System.Drawing.Point(-1, -1);
      this.BtnChart.Name = "BtnChart";
      this.BtnChart.Size = new System.Drawing.Size(75, 26);
      this.BtnChart.TabIndex = 0;
      this.BtnChart.Text = "Charts";
      this.BtnChart.UseVisualStyleBackColor = false;
      this.BtnChart.Click += new System.EventHandler(this.BtnChart_Click);
      // 
      // PanelContent
      // 
      this.PanelContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.PanelContent.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
      this.PanelContent.Controls.Add(this.panel1);
      this.PanelContent.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PanelContent.Location = new System.Drawing.Point(0, 26);
      this.PanelContent.Name = "PanelContent";
      this.PanelContent.Size = new System.Drawing.Size(1184, 735);
      this.PanelContent.TabIndex = 1;
      // 
      // panel1
      // 
      this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel1.Controls.Add(this.lblWrong);
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.btnExit);
      this.panel1.Controls.Add(this.btnLogIn);
      this.panel1.Controls.Add(this.txtPassword);
      this.panel1.Controls.Add(this.txtUsername);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Location = new System.Drawing.Point(410, 116);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(400, 415);
      this.panel1.TabIndex = 0;
      // 
      // lblWrong
      // 
      this.lblWrong.AutoSize = true;
      this.lblWrong.ForeColor = System.Drawing.Color.Red;
      this.lblWrong.Location = new System.Drawing.Point(118, 106);
      this.lblWrong.MaximumSize = new System.Drawing.Size(289, 0);
      this.lblWrong.Name = "lblWrong";
      this.lblWrong.Size = new System.Drawing.Size(149, 13);
      this.lblWrong.TabIndex = 8;
      this.lblWrong.Text = "Wrong username or Password";
      this.lblWrong.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblWrong.Visible = false;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(30, 206);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Password:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(28, 147);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(58, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Username:";
      // 
      // btnExit
      // 
      this.btnExit.BackgroundBrush = new MindFusion.Drawing.SolidBrush("#FFF0F0F0");
      this.btnExit.BackgroundBrushDisabled = new MindFusion.Drawing.SolidBrush("#FFDEDEDE");
      this.btnExit.BackgroundBrushDown = new MindFusion.Drawing.SolidBrush("#FFAEAEAE");
      this.btnExit.BackgroundBrushOver = new MindFusion.Drawing.SolidBrush("#FFC5C5C5");
      this.btnExit.BorderBrush = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnExit.BorderBrushDisabled = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnExit.BorderBrushDown = new MindFusion.Drawing.SolidBrush("#FF777777");
      this.btnExit.BorderBrushOver = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnExit.BorderThickness = 0;
      this.btnExit.ForegroundBrush = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnExit.ForegroundBrushDisabled = new MindFusion.Drawing.SolidBrush("#FF777777");
      this.btnExit.ForegroundBrushDown = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnExit.ForegroundBrushOver = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnExit.Location = new System.Drawing.Point(215, 264);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 23);
      this.btnExit.TabIndex = 3;
      this.btnExit.Text = "Exit";
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // btnLogIn
      // 
      this.btnLogIn.BackgroundBrush = new MindFusion.Drawing.SolidBrush("#FFF0F0F0");
      this.btnLogIn.BackgroundBrushDisabled = new MindFusion.Drawing.SolidBrush("#FFDEDEDE");
      this.btnLogIn.BackgroundBrushDown = new MindFusion.Drawing.SolidBrush("#FFAEAEAE");
      this.btnLogIn.BackgroundBrushOver = new MindFusion.Drawing.SolidBrush("#FFC5C5C5");
      this.btnLogIn.BorderBrush = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnLogIn.BorderBrushDisabled = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnLogIn.BorderBrushDown = new MindFusion.Drawing.SolidBrush("#FF777777");
      this.btnLogIn.BorderBrushOver = new MindFusion.Drawing.SolidBrush("#FFA6A6A6");
      this.btnLogIn.BorderThickness = 0;
      this.btnLogIn.ForegroundBrush = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnLogIn.ForegroundBrushDisabled = new MindFusion.Drawing.SolidBrush("#FF777777");
      this.btnLogIn.ForegroundBrushDown = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnLogIn.ForegroundBrushOver = new MindFusion.Drawing.SolidBrush("#FF000000");
      this.btnLogIn.Location = new System.Drawing.Point(103, 264);
      this.btnLogIn.Name = "btnLogIn";
      this.btnLogIn.Size = new System.Drawing.Size(75, 23);
      this.btnLogIn.TabIndex = 2;
      this.btnLogIn.Text = "Log-In";
      this.btnLogIn.Click += new System.EventHandler(this.btnLogIn_Click);
      // 
      // txtPassword
      // 
      this.txtPassword.Location = new System.Drawing.Point(103, 203);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.Size = new System.Drawing.Size(187, 20);
      this.txtPassword.TabIndex = 1;
      this.txtPassword.UseSystemPasswordChar = true;
      // 
      // txtUsername
      // 
      this.txtUsername.Location = new System.Drawing.Point(103, 144);
      this.txtUsername.Name = "txtUsername";
      this.txtUsername.Size = new System.Drawing.Size(187, 20);
      this.txtUsername.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(125, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(142, 55);
      this.label1.TabIndex = 1;
      this.label1.Text = "Login";
      // 
      // Index
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1184, 761);
      this.Controls.Add(this.PanelContent);
      this.Controls.Add(this.PanelMenu);
      this.MinimumSize = new System.Drawing.Size(1200, 800);
      this.Name = "Index";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Covid Reporter";
      this.PanelMenu.ResumeLayout(false);
      this.PanelContent.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel PanelMenu;
    private System.Windows.Forms.Button BtnChart;
    private System.Windows.Forms.Button BntTables;
    private System.Windows.Forms.Panel PanelContent;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label2;
    private MindFusion.UI.WinForms.Button btnExit;
    private MindFusion.UI.WinForms.Button btnLogIn;
    private System.Windows.Forms.TextBox txtPassword;
    private System.Windows.Forms.TextBox txtUsername;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label lblWrong;
  }
}