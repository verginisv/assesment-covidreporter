﻿using CovidReporterClient.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CovidReporterClient.Pages
{
  public partial class Tables : Form
  {
    System.Timers.Timer timerWrong;
    private BindingSource bindingSource;
    DataTable Table;
    public Tables()
    {
      InitializeComponent();
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      cmbNUTS.DataSource = Index.nutsValues;

      bindingSource = new BindingSource();
      GridTable.DataSource = bindingSource;
      Table = new DataTable();

    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      try
      {
        TableReq req = new TableReq();

        if (cmbNUTS.Enabled)
        {
          req.Nuts = cmbNUTS.SelectedItem.ToString();
        }
        if (dtpTo.Enabled && dtpFrom.Enabled)
        {
          if (dtpTo.Value < dtpFrom.Value)
          {
            StartWronglbl("Wrong input dates");
            return;
          }
          req.From = dtpFrom.Value;
          req.To = dtpTo.Value;
        }

        CovidData bsl = new CovidData();
        var list = bsl.PopulateTable(req);

        if (list.Count == 0) { StartWronglbl("No Records Found"); return; }

        Table = list.ToDataTable();


        bindingSource.DataSource = Table;

      }
      catch (Exception) { StartWronglbl("Unknown Error"); return; }
    }


    private void UpdateDataGridView(List<CovidData> list)
    {
      GridTable.DataSource = null; 
      GridTable.DataSource = list;
      GridTable.AutoResizeColumns();
    }
    private void StartWronglbl(string msg = "")
    {
      lblWrong.Visible = true;
      lblWrong.Text = msg;
      timerWrong = new System.Timers.Timer(3000);
      timerWrong.Elapsed += OnTimerElapsedWrong;
      timerWrong.AutoReset = false;
      timerWrong.Start();
    }
    private void OnTimerElapsedWrong(object sender, System.Timers.ElapsedEventArgs e)
    {
      if (lblWrong.InvokeRequired)
      {
        lblWrong.Invoke(new Action(() => OnTimerElapsedWrong(sender, e)));
      }
      else
      {
        // This code runs on the UI thread
        lblWrong.Visible = false;
        timerWrong.Dispose();
      }
    }

    #region Events
    private void dtpTables_KeyPress(object sender, KeyPressEventArgs e)
    {
      e.Handled = true;
    }
    private void chkDates_CheckStateChanged(object sender, EventArgs e)
    {
      dtpFrom.Enabled = !dtpFrom.Enabled;
      dtpTo.Enabled = !dtpTo.Enabled;
    }
    private void chkNuts_CheckStateChanged(object sender, EventArgs e)
    {
      cmbNUTS.Enabled = !cmbNUTS.Enabled;
    }

    #endregion

    
  }
}
