﻿
namespace CovidReporterClient.Pages
{
  partial class Tables
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cmbNUTS = new System.Windows.Forms.ComboBox();
      this.dtpTo = new System.Windows.Forms.DateTimePicker();
      this.btnSearch = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lblWrong = new System.Windows.Forms.Label();
      this.chkDates = new System.Windows.Forms.CheckBox();
      this.chkNuts = new System.Windows.Forms.CheckBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.dtpFrom = new System.Windows.Forms.DateTimePicker();
      this.GridTable = new System.Windows.Forms.DataGridView();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.GridTable)).BeginInit();
      this.SuspendLayout();
      // 
      // cmbNUTS
      // 
      this.cmbNUTS.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.cmbNUTS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbNUTS.Enabled = false;
      this.cmbNUTS.FormattingEnabled = true;
      this.cmbNUTS.Location = new System.Drawing.Point(42, 5);
      this.cmbNUTS.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
      this.cmbNUTS.Name = "cmbNUTS";
      this.cmbNUTS.Size = new System.Drawing.Size(82, 21);
      this.cmbNUTS.TabIndex = 0;
      // 
      // dtpTo
      // 
      this.dtpTo.CalendarMonthBackground = System.Drawing.SystemColors.ButtonFace;
      this.dtpTo.Enabled = false;
      this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dtpTo.Location = new System.Drawing.Point(286, 6);
      this.dtpTo.MaxDate = new System.DateTime(2023, 5, 2, 0, 0, 0, 0);
      this.dtpTo.MinDate = new System.DateTime(2020, 1, 3, 0, 0, 0, 0);
      this.dtpTo.Name = "dtpTo";
      this.dtpTo.Size = new System.Drawing.Size(82, 20);
      this.dtpTo.TabIndex = 1;
      this.dtpTo.Value = new System.DateTime(2023, 5, 2, 0, 0, 0, 0);
      this.dtpTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpTables_KeyPress);
      // 
      // btnSearch
      // 
      this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.btnSearch.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSearch.Location = new System.Drawing.Point(722, 5);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(75, 20);
      this.btnSearch.TabIndex = 2;
      this.btnSearch.Text = "Search";
      this.btnSearch.UseVisualStyleBackColor = false;
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.lblWrong);
      this.panel1.Controls.Add(this.chkDates);
      this.panel1.Controls.Add(this.chkNuts);
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.dtpFrom);
      this.panel1.Controls.Add(this.cmbNUTS);
      this.panel1.Controls.Add(this.btnSearch);
      this.panel1.Controls.Add(this.dtpTo);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(800, 31);
      this.panel1.TabIndex = 3;
      // 
      // lblWrong
      // 
      this.lblWrong.AutoSize = true;
      this.lblWrong.ForeColor = System.Drawing.Color.Red;
      this.lblWrong.Location = new System.Drawing.Point(532, 9);
      this.lblWrong.MaximumSize = new System.Drawing.Size(289, 0);
      this.lblWrong.Name = "lblWrong";
      this.lblWrong.Size = new System.Drawing.Size(94, 13);
      this.lblWrong.TabIndex = 9;
      this.lblWrong.Text = "Wrong input dates";
      this.lblWrong.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblWrong.Visible = false;
      // 
      // chkDates
      // 
      this.chkDates.AutoSize = true;
      this.chkDates.Location = new System.Drawing.Point(450, 7);
      this.chkDates.Name = "chkDates";
      this.chkDates.Size = new System.Drawing.Size(76, 17);
      this.chkDates.TabIndex = 8;
      this.chkDates.Text = "Use Dates";
      this.chkDates.UseVisualStyleBackColor = true;
      this.chkDates.CheckStateChanged += new System.EventHandler(this.chkDates_CheckStateChanged);
      // 
      // chkNuts
      // 
      this.chkNuts.AutoSize = true;
      this.chkNuts.Location = new System.Drawing.Point(374, 8);
      this.chkNuts.Name = "chkNuts";
      this.chkNuts.Size = new System.Drawing.Size(70, 17);
      this.chkNuts.TabIndex = 7;
      this.chkNuts.Text = "Use Nuts";
      this.chkNuts.UseVisualStyleBackColor = true;
      this.chkNuts.CheckStateChanged += new System.EventHandler(this.chkNuts_CheckStateChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(257, 9);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(23, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "To:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(130, 8);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(33, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "From:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(37, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "NUTS";
      // 
      // dtpFrom
      // 
      this.dtpFrom.CalendarMonthBackground = System.Drawing.SystemColors.ButtonFace;
      this.dtpFrom.Enabled = false;
      this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dtpFrom.Location = new System.Drawing.Point(169, 6);
      this.dtpFrom.MaxDate = new System.DateTime(2023, 5, 2, 0, 0, 0, 0);
      this.dtpFrom.MinDate = new System.DateTime(2020, 1, 3, 0, 0, 0, 0);
      this.dtpFrom.Name = "dtpFrom";
      this.dtpFrom.Size = new System.Drawing.Size(82, 20);
      this.dtpFrom.TabIndex = 3;
      this.dtpFrom.Value = new System.DateTime(2023, 5, 2, 0, 0, 0, 0);
      // 
      // GridTable
      // 
      this.GridTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.GridTable.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
      this.GridTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.GridTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GridTable.Location = new System.Drawing.Point(0, 31);
      this.GridTable.Name = "GridTable";
      this.GridTable.Size = new System.Drawing.Size(800, 419);
      this.GridTable.TabIndex = 4;
      this.GridTable.VirtualMode = true;
      // 
      // Tables
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.GridTable);
      this.Controls.Add(this.panel1);
      this.Name = "Tables";
      this.Text = "Tables";
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.GridTable)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cmbNUTS;
    private System.Windows.Forms.DateTimePicker dtpTo;
    private System.Windows.Forms.Button btnSearch;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.DataGridView GridTable;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.DateTimePicker dtpFrom;
    private System.Windows.Forms.CheckBox chkDates;
    private System.Windows.Forms.CheckBox chkNuts;
    private System.Windows.Forms.Label lblWrong;
  }
}