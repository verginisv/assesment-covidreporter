﻿
namespace CovidReporterClient.Pages
{
  partial class Charts
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.CovidDataDashboard = new MindFusion.Charting.WinForms.Dashboard();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnSearch = new System.Windows.Forms.Button();
      this.cmbCountries = new System.Windows.Forms.ComboBox();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // CovidDataDashboard
      // 
      this.CovidDataDashboard.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.CovidDataDashboard.Dock = System.Windows.Forms.DockStyle.Fill;
      this.CovidDataDashboard.Location = new System.Drawing.Point(0, 0);
      this.CovidDataDashboard.Name = "CovidDataDashboard";
      this.CovidDataDashboard.Size = new System.Drawing.Size(1184, 761);
      this.CovidDataDashboard.TabIndex = 0;
      this.CovidDataDashboard.Text = "dashboard1";
      this.CovidDataDashboard.Theme.UniformSeriesStroke = new MindFusion.Drawing.SolidBrush("#FF5A5A5A");
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnSearch);
      this.panel1.Controls.Add(this.cmbCountries);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1184, 31);
      this.panel1.TabIndex = 1;
      // 
      // btnSearch
      // 
      this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.btnSearch.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSearch.Location = new System.Drawing.Point(1106, 1);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(75, 23);
      this.btnSearch.TabIndex = 3;
      this.btnSearch.Text = "Search";
      this.btnSearch.UseVisualStyleBackColor = false;
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // cmbCountries
      // 
      this.cmbCountries.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cmbCountries.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.cmbCountries.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbCountries.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.cmbCountries.FormattingEnabled = true;
      this.cmbCountries.Location = new System.Drawing.Point(3, 3);
      this.cmbCountries.Name = "cmbCountries";
      this.cmbCountries.Size = new System.Drawing.Size(121, 21);
      this.cmbCountries.TabIndex = 2;
      // 
      // Charts
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.ClientSize = new System.Drawing.Size(1184, 761);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.CovidDataDashboard);
      this.Name = "Charts";
      this.Text = "Charts";
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private MindFusion.Charting.WinForms.Dashboard CovidDataDashboard;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ComboBox cmbCountries;
    private System.Windows.Forms.Button btnSearch;
  }
}