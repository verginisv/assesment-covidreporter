﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace CovidReporterClient.Pages
{
  public partial class Index : Form
  {
    System.Timers.Timer timerWrong;
    Charts chart;
    Tables tablePage;
    static public List<string> nutsValues;
    public Index()
    {
      InitializeComponent();
     
    }

    #region Buttons
    private void BtnChart_Click(object sender, EventArgs e)
    {
      if (!LoginState.Instance.GetStatus())
      {
        StartWronglbl("Need to Login First");
        return;
      }

      if (this.chart == null)
        this.chart = new Charts();

      chart.TopLevel = false;

      if (PanelContent.Controls.Count > 0)
        PanelContent.Controls.Clear();
      PanelContent.Controls.Add(this.chart);

      chart.Dock = DockStyle.Fill;
      chart.BringToFront();
      chart.Show();
    }
    private void BntTables_Click(object sender, EventArgs e)
    {
      if (!LoginState.Instance.GetStatus())
      {
        StartWronglbl("Need to Login First");
        return;
      }

      if (this.tablePage == null)
        this.tablePage = new Tables();

      tablePage.TopLevel = false;

      if (PanelContent.Controls.Count > 0)
        PanelContent.Controls.Clear();
      PanelContent.Controls.Add(this.tablePage);

      tablePage.Dock = DockStyle.Fill;
      tablePage.BringToFront();
      tablePage.Show();
    }
    private void btnExit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
    private void btnLogIn_Click(object sender, EventArgs e)
    {
      try
      {
        if (string.IsNullOrWhiteSpace(txtUsername.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
        {
          StartWronglbl("Empty username or Password");
          return;
        }

        Login loginImplementation = new Login();
        bool res = loginImplementation.LogOn(txtUsername.Text, txtPassword.Text);

        if (!res) { StartWronglbl("Wrong username or Password"); return; }

        LoginState.Instance.ChangeLogInStatus();
        InitNUTS();
        RenderIndex();
      }
      catch (Exception) { throw; }
    }
    #endregion


    private void RenderIndex()
    {
      this.chart = new Charts();
      chart.TopLevel = false;

      if (PanelContent.Controls.Count > 0)
        PanelContent.Controls.Clear();
      PanelContent.Controls.Add(this.chart);

      chart.Dock = DockStyle.Fill;
      chart.BringToFront();
      chart.Show();
    }
    private void StartWronglbl(string msg)
    {
      lblWrong.Visible = true;
      lblWrong.Text = msg;
      timerWrong = new System.Timers.Timer(3000);
      timerWrong.Elapsed += OnTimerElapsedWrong;
      timerWrong.AutoReset = false;
      timerWrong.Start();
    }
    private void OnTimerElapsedWrong(object sender, System.Timers.ElapsedEventArgs e)
    {
      if (lblWrong.InvokeRequired)
      {
        lblWrong.Invoke(new Action(() => OnTimerElapsedWrong(sender, e)));
      }
      else
      {
        // This code runs on the UI thread
        lblWrong.Visible = false;
        timerWrong.Dispose();
      }
    }
    private void InitNUTS()
    {
      nutsValues = new List<string>()
      {
        "SI","CY","BE","LV","FR","FI","SE","EL","AT","PT","RO","ES","EE","LU",
        "NL","DE","HR","DK","IE","SK","PL","IT","HU","BG","LT","MT"
      };

    }

  }
}
