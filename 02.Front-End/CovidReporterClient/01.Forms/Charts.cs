﻿using MindFusion.Charting;
using MindFusion.Charting.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidBrush = MindFusion.Drawing.SolidBrush;
using Brush = MindFusion.Drawing.Brush;
using MindFusion.Charting.WinForms;


namespace CovidReporterClient.Pages
{
  public partial class Charts : Form
  {
    public Charts()
    {
      InitializeComponent();
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      cmbCountries.DataSource = Index.nutsValues;
      RenderChartPage();
    }

    PieRenderer pieRenderer;
    PieSeries PieSeries;
    GridPanel Grid;
    PolarPlot PiePlot;
    List<Brush> FillsBrushList;
    List<Brush> StrokeBrushList;
    List<double> Thicknesses;
    List<List<Brush>> SeriesFillsBrush;
    List<List<Brush>> SeriesStrokesBrush;
    List<List<double>> SeriesThicknesses;
    PerElementSeriesStyle PESS;
    LayoutBuilder builder;
    Color cream = Color.FromArgb(248, 224, 222);


    List<double> PieValues = new List<double>();
    List<string> PieInnerLabels = new List<string>();
    List<string> PieOuterLabels = new List<string>();

    List<double> BarValues = new List<double>() { 0, 0, 0, 0, 0 };
    List<string> BarInnerLabels = new List<string>() { "Pre-Covid", "Year", "Year", "Year", "Year" };
    List<string> BarOuterLabels = new List<string>() { "2020", "2021", "2022", "2023", "Year" };

    List<double> LineValues = new List<double>() { 0, 0, 0, 0, 0 };
    List<double> LineXAxis = new List<double>() { 0, 1, 2, 3, 4 };


    private void RenderChartPage()
    {
      InitBrushes();
      InitGrid();
      InitPieChart();
      InitBarChart();
      InitLineChart();
    }
    private void InitBrushes()
    {
      FillsBrushList = new List<Brush>()
      {
          new SolidBrush(Color.FromArgb(255, 99, 132)),   // Red
          new SolidBrush(Color.FromArgb(54, 162, 235)),   // Blue
          new SolidBrush(Color.FromArgb(255, 206, 86)),   // Yellow
          new SolidBrush(Color.FromArgb(75, 192, 192)),   // Teal
          new SolidBrush(Color.FromArgb(153, 102, 255)),  // Purple
          new SolidBrush(Color.FromArgb(255, 159, 64)),   // Orange
          new SolidBrush(Color.FromArgb(255, 99, 71)),    // Tomato
          new SolidBrush(Color.FromArgb(46, 139, 87)),    // SeaGreen
          new SolidBrush(Color.FromArgb(70, 130, 180)),   // SteelBlue
          new SolidBrush(Color.FromArgb(255, 20, 147)),   // DeepPink
          new SolidBrush(Color.FromArgb(255, 140, 0)),    // DarkOrange
          new SolidBrush(Color.FromArgb(255, 215, 0)),    // Gold
          new SolidBrush(Color.FromArgb(50, 205, 50)),    // LimeGreen
          new SolidBrush(Color.FromArgb(138, 43, 226)),   // BlueViolet
          new SolidBrush(Color.FromArgb(210, 105, 30)),   // Chocolate
          new SolidBrush(Color.FromArgb(154, 205, 50)),   // YellowGreen
          new SolidBrush(Color.FromArgb(255, 69, 0)),     // OrangeRed
          new SolidBrush(Color.FromArgb(64, 224, 208)),   // Turquoise
          new SolidBrush(Color.FromArgb(123, 104, 238)),  // MediumSlateBlue
          new SolidBrush(Color.FromArgb(0, 191, 255)),    // DeepSkyBlue
          new SolidBrush(Color.FromArgb(238, 130, 238)),  // Violet
          new SolidBrush(Color.FromArgb(218, 112, 214)),  // Orchid
          new SolidBrush(Color.FromArgb(255, 105, 180)),  // HotPink
          new SolidBrush(Color.FromArgb(72, 61, 139)),    // DarkSlateBlue
          new SolidBrush(Color.FromArgb(107, 142, 35)),   // OliveDrab
          new SolidBrush(Color.FromArgb(0, 255, 127))     // SpringGreen
      };
      StrokeBrushList = new List<Brush>() { new SolidBrush(Color.FromKnownColor(KnownColor.GradientActiveCaption)) };
      Thicknesses = new List<double>() { 1.0 };

      SeriesFillsBrush = new List<List<Brush>>() { FillsBrushList };
      SeriesStrokesBrush = new List<List<Brush>>() { StrokeBrushList };
      SeriesThicknesses = new List<List<double>>() { Thicknesses };

      PESS = new PerElementSeriesStyle(SeriesFillsBrush, SeriesStrokesBrush, SeriesThicknesses, null);
    }
    private void InitGrid()
    {
      CovidDataDashboard.LayoutPanel.Children.Add(new TextComponent()
      {
        Text = "Covid Data Charts",
        TextBrush = new MindFusion.Drawing.SolidBrush(cream),
        FontName = "Times New Roman",
        //Margin = new MindFusion.Charting.Margins(10),
        Margin = new MindFusion.Charting.Margins(10, 30, 10, 10),
        FontStyle = FontStyle.Bold,
        FontSize = 20,
        HorizontalAlignment = LayoutAlignment.Center
      });

      if (this.Grid == null)
      {
        Grid = new GridPanel();
        Grid.Columns.Add(new GridColumn());
        Grid.Columns.Add(new GridColumn());

        foreach (var col in Grid.Columns)
        {
          col.LengthType = MindFusion.Charting.Components.LengthType.Relative;
        }
        Grid.Rows.Add(new GridRow());

        CovidDataDashboard.LayoutPanel.Children.Add(Grid);

        if (builder == null)
          builder = new LayoutBuilder(CovidDataDashboard);
        //CovidDataDashboard.LayoutPanel.Children.Add(test);
      }
    }
    private void InitPieChart()
    {

        this.PieSeries = new PieSeries(PieValues, PieInnerLabels, PieOuterLabels);
        this.pieRenderer = new PieRenderer(PieSeries);
         this.PiePlot = new PolarPlot();
        PiePlot.SeriesRenderers.Add(pieRenderer);
        PiePlot.SeriesStyle = PESS;
      

      BorderComponent pieBorder = new BorderComponent();
        pieBorder.BorderThickness = 8;
        pieBorder.BorderBrush = new SolidBrush(Color.FromKnownColor(KnownColor.GradientActiveCaption));
        pieBorder.Margin = new Margins(5, 0, 0, 5);
        pieBorder.GridColumn = 0;
        pieBorder.GridRow = 1;
        pieBorder.Content = PiePlot;

        var pieTitle = new TextComponent()
        {
          Text = "Outbreaks/Deaths",
          TextBrush = new MindFusion.Drawing.SolidBrush(cream),
          FontName = "Times New Roman",
          FontSize = 15,
          HorizontalAlignment = LayoutAlignment.Center,
          GridRow = 0,
          GridColumn = 0
        };

        Grid.Children.Add(pieBorder);
        Grid.Children.Add(pieTitle);
     
    }
    private void InitBarChart()
    {
      Axis xAxis = new Axis();
      xAxis.MinValue = 0;
      xAxis.MaxValue = BarValues.Max(x => x)+1000;
      xAxis.Interval = BarValues.Max(x => x) / 4;
      xAxis.Title = "Outbreaks";


      Axis yAxis = new Axis();
      yAxis.MinValue = -1;//-1;
      yAxis.MaxValue = 4;
      yAxis.Interval = 1;
      yAxis.Title = "Year";

      YAxisRenderer yRenderer = new YAxisRenderer(yAxis);
      yRenderer.ShowCoordinates = false;
      yRenderer.ShowTicks = false;
      XAxisRenderer xRenderer = new XAxisRenderer(xAxis);
      xRenderer.ShowCoordinates = false;
      yRenderer.ShowTicks = false;


      BarSeries bSeries = new BarSeries(BarValues, BarInnerLabels, BarOuterLabels);
    



      var barRenderer = new BarRenderer(new System.Collections.ObjectModel.ObservableCollection<Series>() { bSeries });

      barRenderer.YAxis = yAxis;
      barRenderer.XAxis = xAxis;
      barRenderer.HorizontalBars = true;
      barRenderer.BarSpacingRatio = 0.1;
      barRenderer.SeriesStyle = PESS;
      barRenderer.LabelBrush = new SolidBrush(Color.Black);

      Plot2D barPlot = new Plot2D();
      barPlot.XAxis = xAxis;
      barPlot.YAxis = yAxis;
      barPlot.BorderStroke = new SolidBrush(Color.LightBlue);
      barPlot.GridType = GridType.Vertical;
      barPlot.GridLineColor = Color.LightBlue;
      barPlot.GridColor1 = barPlot.GridColor2 = Color.Transparent;
      barPlot.GridLineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
      barPlot.SeriesRenderers.Add(barRenderer);



      var barPlotWithAxes = builder.CreatePlotWithBottomAndLeftAxes(barPlot, xRenderer, yRenderer);

      BorderComponent barBorder = new BorderComponent();
      barBorder.BorderThickness = 8;
      barBorder.BorderBrush = new SolidBrush(Color.FromKnownColor(KnownColor.GradientActiveCaption));
      barBorder.Margin = new Margins(5, 0, 0, 5);
      barBorder.GridColumn = 1;
      barBorder.GridRow = 1;
      barBorder.Padding = 15;
      barBorder.Content = barPlotWithAxes;


      var barTitle = new TextComponent()
      {
        Text = "Outbreaks/Year",
        TextBrush = new MindFusion.Drawing.SolidBrush(cream),
        FontName = "Times New Roman",
        FontSize = 15,
        HorizontalAlignment = LayoutAlignment.Center,
        GridRow = 0,
        GridColumn = 1
      };

      Grid.Children.Add(barBorder);
      Grid.Children.Add(barTitle);
    }
    private void InitLineChart()
    {
      Axis arePlotYAxis = new Axis();
      arePlotYAxis.MinValue = 0;
      arePlotYAxis.MaxValue = LineValues[LineValues.Count-1];
      arePlotYAxis.Interval = LineValues[LineValues.Count - 1] / 4;
      arePlotYAxis.Title = "Deaths";

      Axis arePlotXAxis = new Axis();
      arePlotXAxis.MinValue = 2020;
      arePlotXAxis.MaxValue = 2023;
      arePlotXAxis.Interval = 1;
      arePlotXAxis.Title = "Year";

      var yRenderer = new YAxisRenderer(arePlotYAxis);
      var xRenderer = new XAxisRenderer(arePlotXAxis);
       
      Series2D series1 = new Series2D(LineXAxis, LineValues, null);

      series1.Title = "Increments / Year";
      series1.SupportedLabels = LabelKinds.XAxisLabel;



      var areaRenderer = new AreaRenderer(new System.Collections.ObjectModel.ObservableCollection<Series>() { series1 });

      areaRenderer.YAxis = arePlotYAxis;
      areaRenderer.XAxis = arePlotXAxis;


      Plot2D areaPlot = new Plot2D();
      areaPlot.XAxis = arePlotXAxis;
      areaPlot.YAxis = arePlotYAxis;
      areaPlot.BorderStroke = new SolidBrush(Color.LightBlue);
      areaPlot.GridType = GridType.Crossed;
      areaPlot.GridLineColor = Color.LightBlue;
      areaPlot.GridColor1 = areaPlot.GridColor2 = Color.Transparent;
      areaPlot.GridLineThickness = 1;
      areaPlot.SeriesRenderers.Add(areaRenderer);

      xRenderer.LabelsSource = areaPlot;

      //  var builder1 = new LayoutBuilder(CovidDataDashboard); // Ensure builder is properly initialized
      var areaPlotWithAxes = builder.CreatePlotWithBottomAndLeftAxes(areaPlot, xRenderer, yRenderer);

      BorderComponent areaBorder = new BorderComponent();
      areaBorder.BorderThickness = 8;
      areaBorder.BorderBrush = new SolidBrush(Color.FromKnownColor(KnownColor.GradientActiveCaption));
      areaBorder.Margin = new Margins(5, 0, 5, 5);
      areaBorder.Padding = 15;
      areaBorder.GridColumn = 2;
      areaBorder.GridRow = 1;
      areaBorder.Content = areaPlotWithAxes;

      var barTitle = new TextComponent()
      {
        Text = "Death/Year",
        TextBrush = new MindFusion.Drawing.SolidBrush(cream),
        FontName = "Times New Roman",
        FontSize = 15,
        HorizontalAlignment = LayoutAlignment.Center,
        GridRow = 0,
        GridColumn = 2
      };


      Grid.Children.Add(areaBorder);
      Grid.Children.Add(barTitle);
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      try
      {
        string req = cmbCountries.SelectedItem.ToString();

        if (string.IsNullOrWhiteSpace(req))
          return;


        CovidData bls = new CovidData();
        Dashboard res = bls.GetDashboard(req);


        List<double> LYValues = new List<double>() { 0 };
        List<double> LXValues = new List<double>() { 0 };

        foreach (var row in res.LineChart)
        {
          LYValues.Add(row.Deaths);
          LXValues.Add(row.Year);
        }
        LineValues = LYValues.OrderBy(o => o).ToList();
        LineXAxis = LXValues.OrderBy(o => o).ToList();


        List<double> BValues = new List<double>();
        List<string> BIL = new List<string>();

        res.BarChart = res.BarChart.OrderBy((o) => o.Year).ToList();

        foreach(var row in res.BarChart)
        {
          BValues.Add(row.Outbreaks);
          BIL.Add(row.Outbreaks.ToString());

        }

        BarValues = BValues;
        BarInnerLabels = BIL;


        List<double> PValues = new List<double>();
        List<string> PIL = new List<string>();
        List<string> POL = new List<string>();


        PValues.Add(res.PieChart.Outbreaks);
        PIL.Add(res.PieChart.Outbreaks.ToString());
        POL.Add("OBreaks");
        
        PValues.Add(res.PieChart.Deaths);
        PIL.Add(res.PieChart.Deaths.ToString());
        POL.Add("Deaths");


        PieValues = PValues;
        PieInnerLabels = PIL;
        PieOuterLabels = POL;

        Grid.Children.Clear();
        InitLineChart();
        InitBarChart();
        InitPieChart();

      }
      catch (Exception ex) { throw; }
    }
  }
}
