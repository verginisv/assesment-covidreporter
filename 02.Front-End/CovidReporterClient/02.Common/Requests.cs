﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CovidReporterClient.Common
{
  public enum UrlEnum
  {
    Login,
    Table,
    GetDashboard

  }

  public sealed class Requests
  {
    #region Singleton
    private static readonly Lazy<Requests> _instance = new Lazy<Requests>(() => new Requests());
    public static Requests Instance => _instance.Value;
    private Requests()
    {

    }
    #endregion



    private string GetUrl(UrlEnum url, string data)
    {
      string urlString = "";
      switch (url)
      {
        case UrlEnum.Login:
          urlString = $"http://localhost:61720/api/Account/Login?" + data;
          break;
        case UrlEnum.Table:
          urlString = $"http://localhost:61720/api/CovidData/GetTable";
          break;
        case UrlEnum.GetDashboard:
          urlString = $"http://localhost:61720/api/CovidData/GetDashboard?" + data;
          break;
      }
      return urlString;
    }
    public OT Post<IT, OT>(UrlEnum url, IT data)
    {
      try
      {
        string inputData = JsonConvert.SerializeObject(data);
        string resp = WRequest(url, "POST", inputData);

        OT response = JsonConvert.DeserializeObject<OT>(resp);
        return response;
      }
      catch (Exception ex) { throw ex; }
    }
    public OT Get<OT>(UrlEnum url, string data)
    {
      try
      {
        var resp = WRequest(url, "GET", data);
        OT response = JsonConvert.DeserializeObject<OT>(resp);
        return response;
      }
      catch (Exception ex) { throw ex; }
    }


    private string WRequest(UrlEnum url, string method, string POSTdata)
    {
      string responseData = "";

      try
      {
        HttpWebRequest hwrequest;
        if (method == "POST")
        {
          hwrequest = (HttpWebRequest)WebRequest.Create(GetUrl(url, null));
        }
        else
        {
          hwrequest = (HttpWebRequest)WebRequest.Create(GetUrl(url, POSTdata));
        }

        hwrequest.AllowAutoRedirect = false;
        hwrequest.UserAgent = "http_requester/0.1";
        //hwrequest.Timeout = 10000;
        hwrequest.Method = method;


        if (hwrequest.Method == "POST")
        {
          hwrequest.ContentType = "application/json; charset=utf-8";
          Encoding encoding = new UTF8Encoding();
          byte[] postByteArray = encoding.GetBytes(POSTdata);
          hwrequest.ContentLength = postByteArray.Length;
          Stream postStream = hwrequest.GetRequestStream();
          postStream.Write(postByteArray, 0, postByteArray.Length);
          postStream.Close();
        }

        HttpWebResponse hwresponse = (HttpWebResponse)hwrequest.GetResponse();

        if (hwresponse.StatusCode == HttpStatusCode.OK)
        {
          StreamReader responseStream = new StreamReader(hwresponse.GetResponseStream());
          responseData = responseStream.ReadToEnd();
        }
        hwresponse.Close();

      }
      catch (Exception e)
      {
        responseData = "An error occurred: " + e.Message;
      }

      return responseData;
    }


  }

}
