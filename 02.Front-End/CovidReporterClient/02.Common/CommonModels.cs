﻿using System.Net;
using System.Net.Http;

public class GeneralResponse<T>
{
  public GeneralResponse(T data, HttpStatusCode code)
  {
    Contents = data;
    Status = new HttpResponseMessage { StatusCode = code };
  }

  public T Contents { get; set; }
  public HttpResponseMessage Status { get; set; }
}
